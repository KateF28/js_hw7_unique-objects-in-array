// ## Задание
// Реализовать функцию нахождения уникальных объектов в массиве при сравнении 
// с другим массивом, по указанному пользователем свойству.
// Технические требования:

// Написать функцию excludeBy(), которая в качестве первых двух параметров будет принимать 
// на вход два массива с объектами любого типа. В качестве третьего параметра функция должна 
// принимать имя поля, по которому будет проводиться сравнение.
// Функция должна вернуть новый массив, который будет включать те объекты из первого 
// массива, значение указанного свойства которых не встречается среди объектов, 
// представленных во втором массиве. Например, вызов функции 
// excludeBy(peopleList, excluded, 'name') должен возвращать массив из тех пользователей 
// peopleList, имя которых (свойство name) не встречается у пользователей, 
// которые находятся в массиве excluded.
const users = [{
        name: "Ivan",
        surname: "Ivanov",
        gender: "male",
        age: 30
    },
    {
        name: "Anna",
        surname: "Ivanova",
        gender: "female",
        age: 22
    }
];
const users2 = [{
        name: "Ivan",
        surname: "Ivanov",
        gender: "male",
        age: 31
    },
    {
        name: "Smbd",
        surname: "Ivanova",
        gender: "female",
        age: 23
    }
];
let choosedProp = prompt("Choose the property among: name, surname, gender and age", "name");

let excludeBy = (arr, arr2, comparedProp) => {
    let uniqueArr;
    uniqueArr = arr.filter((element) => {
        return !arr2.some((elem) => {
            return elem[comparedProp] === element[comparedProp];
        });
    });
    return uniqueArr;
};

console.log(excludeBy(users, users2, choosedProp));